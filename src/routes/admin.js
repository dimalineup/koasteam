import Router from 'koa-router'

import path from 'path';
import views from 'koa-views';
const serve = require('koa-static');
const router = new Router({
    prefix: '/admin'
})
router.use(views(path.resolve(__dirname, 'admin'), {
    map: {
        html: 'underscore'
    }
}));
router.get('/',  async function(ctx) {
    console.log(path.resolve(__dirname, 'admin'))
    console.log('2')
    return await ctx.render(`admin`);
});



export default router
