import Router from 'koa-router'
import {getSites} from '../model/Sites'
import {getPartners} from '../model/Partners'
import {getWorkers} from '../model/Workers'
import {getTrades} from '../model/Trades'
import {getBots} from '../model/Bots'
import {getSessions} from '../model/Sessions'

const router = new Router({
    prefix: '/adminApi'
})
router.get('/sites', async (ctx, next) => {
    const data = await getSites()
    ctx.body = data;
})

router.get('/partners', async (ctx, next) => {
    const data = await getPartners()
    ctx.body = data;
})
router.get('/workers', async (ctx, next) => {
    const data = await getWorkers()
    ctx.body = data;
})
router.get('/trades', async (ctx, next) => {
    const data = await getTrades()
    ctx.body = data;
})
router.get('/bots',async (ctx, next) => {
    const data = await getBots()
    ctx.body = data;
})
router.get('/sessions', async (ctx, next) => {
    const data = await getSessions()
    ctx.body = data;
})
router.get('/payments/workers', (ctx, next) => {
    ctx.body = 'Hello World!';
})

router.get('/statistic', (ctx, next) => {
    ctx.body = 'Hello World!';
})
router.get('/bots', (ctx, next) => {
    ctx.body = 'Hello World!';
})
router.get('/settings/announcement', (ctx, next) => {
    ctx.body = 'Hello World!';
})
router.get('/tickets', (ctx, next) => {
    ctx.body = 'Hello World!';
})


export default router
