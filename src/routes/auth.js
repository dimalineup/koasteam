import Router from 'koa-router'
import bcrypt from 'bcrypt'
import bodyParser from 'koa-body'

import Token from '../helpers/token'
import User from '../model/User'
import connection from '../db'

const router = new Router({
    prefix: '/admin'
})
router.post('/',  async ctx => {
    ctx.status = 401

    const isAuthorized = await User.isAuthorized(ctx.request.body)

    if (isAuthorized) {
        const tokens = await Token.generatePair(ctx.request.body.username)

        ctx.status = 200
        ctx.body = tokens
    }
})
router.post('/auth', async ctx => {

    const login = ctx.request.body.username;
    const password = ctx.request.body.password;

    if (login && password) {
        //GET ALL ACTIVE USERS FOR PATHWAYS
        try {
      let results =  await connection.query('SELECT * FROM accounts WHERE login ="login"');

           // async function(error, results, fields) {  }



                if (results.length > 0) {

                    //request.session.loggedin = true;
                    //request.session.login = login;
                    //console.log('request.session.login ', request.session.login)
                    const tokens =  await Token.generatePair(ctx.request.body.username)

                    let matchPassword = await  bcrypt.compare(password, results[0].password);
                    if(matchPassword){
                        delete results[0].password;
                        ctx.status = 200
                        ctx.body = { message: 'Auth success',
                            tokens,
                            user: results[0] };
                    }else{
                        ctx.status = 401
                        ctx.body = { message: 'Incorrect Password!', };
                    }

                } else {
                    console.log('login' , results.length)
                    ctx.status = 401
                    ctx.body = { message: 'Username is not exist!' };

                }


        } catch (error) {
            console.log(error);

        }

    } else {
        ctx.status = 401
        ctx.body = { message: 'Username and/or Password empty!'};

    }


});

router.post('/addUser', async ctx => {

    const {error, value} = User.validateCorrect(ctx.request.body);

    if (!error.isEmpty()) {
        return ctx.status(422).json({errors: error.array()});
    }

    const user = {
        login: value.username,
        password: value.password,
        role: 'admin',
        image: '',
        email: value.email,
    }
    try {
        const resultUser = await connection.query('SELECT login, email From accounts where login=? || email=?', [user.login, user.email])


        // async function(errUser, resultUser) {  }
        let hash = await bcrypt.hash(user.password, 10)
        user.password = hash
        if (resultUser.length) {
            ctx.status(401)
            ctx.send({message: 'User Already Exist', email: resultUser[0].email, login: resultUser[0].login});
        } else {

            const result = await connection.query('INSERT INTO accounts SET ?', user)

            //if(err) throw err

            const tokens = await Token.generatePair(ctx.user.username)
            ctx.status(200)
            ctx.body = {
                message: 'User added successfully ',
                tokens,
                user: {
                    id: result.insertId,
                    login: user.login,
                    role: 'admin',
                    image: '',
                    email: user.email,
                }
            };


        }

    } catch (e) {

        ctx.status(401)
        ctx.body = {message: 'Something goes wrong'};

    }

})

/*router.get('/', async ctx => {
    ctx.status = 403

    const { authorization } = ctx.headers
    if (!authorization || !authorization.match(/^Bearer\s/)) return

    const refreshToken = authorization.replace(/^Bearer\s/, '')
    const { username } = await Token.getPayload(refreshToken)
    const hasValidRefreshToken = await User.hasValidRefreshToken(refreshToken)

    if (hasValidRefreshToken) {
        const tokens = await Token.generatePair(username)

        ctx.status = 200
        ctx.body = tokens
    }
})*/

export default router
