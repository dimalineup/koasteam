import connection from '../db'

const getSessions = async () => {
    try {
        const res = await connection.query('SELECT * FROM sessions ')
        return res
    }catch(e){
        console.log('e',e )
        return {errorBase: 1, message: e}
    }
}

const checkAuthSession = async ({sessionKey, id}) => {
    try {
        const res = await connection.query('SELECT * FROM sessions where sessionKey=? AND steamId=?', [sessionKey, id])
        return res
    }catch(e){
        console.log('e',e )
        return {errorBase: 1, message: e}
    }
}

const addSession = async (dataInsert) => {


    try {
        const res = await connection.query('INSERT INTO `sessions` SET ?',dataInsert)
        return res
    }catch(e){
        console.log('e',e )
        return {errorBase: 1, message: e}
    }
}



export {getSessions, addSession, checkAuthSession}


