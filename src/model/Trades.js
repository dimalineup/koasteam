import connection from '../db'

const getTrades = async () => {

    try {
        const res = await connection.query('SELECT * FROM trades ')
        return res
    }catch(e){
        console.log('e',e )
        return {errorBase: 1, message: e}
    }

}
export {getTrades}


