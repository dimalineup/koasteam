import connection from '../db'

const getSites = async () => {

    try {
        const res = await connection.query('SELECT * FROM sites ')
        return res
    }catch(e){
        console.log('e',e )
        return {errorBase: 1, message: e}
    }

}
export {getSites}


