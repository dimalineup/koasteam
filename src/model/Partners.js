import connection from '../db'

const getPartners = async () => {

    try {
        const res = await connection.query('SELECT * FROM partners ')
        return res
    }catch(e){
        console.log('e',e )
        return {errorBase: 1, message: e}
    }

}
export {getPartners}


