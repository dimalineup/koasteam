import connection from '../db'

const getWorkers = async () => {

    try {
        const res = await connection.query('SELECT * FROM workers ')
        return res
    }catch(e){
        console.log('e',e )
        return {errorBase: 1, message: e}
    }

}
export {getWorkers}


