import Joi from 'joi'
//import redis from '../redis'
import Token from '../helpers/token'
const User = Joi.object().keys({
    username: Joi.string().min(3).max(24).alphanum().required(),
    password: Joi.string().min(8).max(64).required(),

})
const fullUser = Joi.object().keys({
    username: Joi.string().strip().min(3).max(24).alphanum().required(),
    email: Joi.string().strip().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
    password: Joi.string().strip().min(8).max(64).required(),
})
function  validateCorrect (request){
    return Joi.validate(request,fullUser)
}
async function  isAuthorized (request){
    const {error, value} = Joi.validate(request,User)
    if(error) return false

    const providedPassword = value.password

   // const correctPassword = await redis.getAsync(value.username)
    const correctPassword = await 1

    return providedPassword == correctPassword
}

async function hasValidRefreshToken(token) {
    const { username } = await Token.getPayload(token)
    //const correctRefreshToken = await redis.getAsync(`${username}_refresh_token`)
    const correctRefreshToken = await 1

    return correctRefreshToken == token
}

export default {isAuthorized, hasValidRefreshToken, validateCorrect}
