import redis from 'redis'
import Promise from "bluebird";
import env from "dotenv";
import Koa from "koa";

env.config()
Promise.promisifyAll(redis.RedisClient.prototype)
Promise.promisifyAll(redis.Multi.prototype)
const app = new Koa()
const db = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
})


export default db
