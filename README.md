#Create TABLES
``` 
CREATE TABLE IF NOT EXISTS sites (
 id int(11) NOT NULL,
 name varchar(50) NOT NULL,
 beloning varchar(50) NOT NULL,
 addedBy varchar(50) NOT NULL,
 design varchar(50) NOT NULL,
 typeDomain varchar(50) NOT NULL,
 assignedTo varchar(50) NOT NULL,
 time  DATE NOT NULL,
 link varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE sites ADD PRIMARY KEY (id);
ALTER TABLE sites MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

CREATE TABLE IF NOT EXISTS partners (
 id int(11) NOT NULL,
 username varchar(50) NOT NULL,
 password varchar(50) NOT NULL,
 status bool NOT NULL,
 lastLogin DATE NOT NULL,
 percent varchar(50) NOT Null,
 image varchar(255),
 email varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE partners ADD PRIMARY KEY (id);
ALTER TABLE partners MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;


CREATE TABLE IF NOT EXISTS accounts (
 id int(11) NOT NULL,
 login varchar(50) NOT NULL,
 password varchar(255) NOT NULL,
 role varchar(50) NOT Null,
 image varchar(255),
 email varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE accounts ADD PRIMARY KEY (id);
ALTER TABLE accounts MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

ALTER TABLE accounts ADD UNIQUE (`login`);
ALTER TABLE accounts ADD UNIQUE (`email`);

CREATE TABLE IF NOT EXISTS workers (
 id int(11) NOT NULL,
 username varchar(50) NOT NULL,
  password varchar(255) NOT NULL,
 status bool NOT NULL,
 ip varchar(50) NOT NULL,
 tradesAmount varchar(50) NOT NULL,
 lastLogin DATETIME NOT NULL,
 partnerBelonging varchar(50) NOT NULL,
 worderInfoId  int(11) NOT NULL,
 image varchar(255),
 email varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE workers ADD PRIMARY KEY (id);
ALTER TABLE workers MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE workers ADD UNIQUE (`username`);
ALTER TABLE workers ADD UNIQUE (`email`);




CREATE TABLE IF NOT EXISTS worderInfo (
 id int(11) NOT NULL,
 general text(255) NOT NULL,
 paymant text(255) NOT NULL,
 tradesWorkerId int(11) NOT NULL,
 sessionsWorkerId int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE worderInfo ADD PRIMARY KEY (id);
ALTER TABLE worderInfo MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

CREATE TABLE IF NOT EXISTS payments (
 id int(11) NOT NULL,
 paymentTime DATETIME NOT NULL,
 amount varchar(50) NOT NULL,
 workerId int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE payments ADD PRIMARY KEY (id);
ALTER TABLE payments MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

CREATE TABLE IF NOT EXISTS sessions (
 id int(11) NOT NULL,
 startTime DATE NOT NULL,
 avatarSrc  varchar(255) NOT NULL,
 status varchar(50) NOT NULL,
 steamId varchar(100) NOT NULL,
 login varchar(50) NOT NULL,
 sessionKey varchar(200) NOT NULL,
 email varchar(50) NOT NULL,
 priceIpventory varchar(50) NOT NULL,
 lastLogin DATETIME NOT NULL,
 beloning varchar(50) NOT NULL,
 ipAddress varchar(50) NOT NULL,
 tradesWorkerId int(11) NOT NULL,
 sessionsWorkerId int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE sessions ADD PRIMARY KEY (id);
ALTER TABLE sessions MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

CREATE TABLE IF NOT EXISTS trades (
 id int(11) NOT NULL,
 siteId int(11) NOT NULL,
 worderId int(11) NOT NULL,
 steam64 varchar(50) NOT NULL,
 sumTrade int(11) NOT NULL,
 items text(255) NOT NULL,
 tradesDateTime DATETIME NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE trades ADD PRIMARY KEY (id);
ALTER TABLE trades MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

CREATE TABLE IF NOT EXISTS botsSteam (
 id int(11) NOT NULL,
 statusBot bool NOT NULL,
 steamId varchar(50) NOT NULL,
 notHolded varchar(50) NOT NULL,
 holded varchar(50) NOT NULL,
 beloning varchar(50) NOT NULL,
 actionBot varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE botsSteam ADD PRIMARY KEY (id);
ALTER TABLE botsSteam MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;


CREATE TABLE IF NOT EXISTS settingsAnnouncement (
 id int(11) NOT NULL,
 topic varchar(50) NOT NULL,
 sendTo varchar(50) NOT NULL,
 dateTime DATETIME NOT NULL,
 workerId int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE settingsAnnouncement ADD PRIMARY KEY (id);
ALTER TABLE settingsAnnouncement MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;


CREATE TABLE IF NOT EXISTS tickets (
 id int(11) NOT NULL,
 topic varchar(50) NOT NULL,
 sendTo varchar(50) NOT NULL,
 dateTime DATETIME NOT NULL,
 workerId int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
ALTER TABLE tickets ADD PRIMARY KEY (id);
ALTER TABLE tickets MODIFY id int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
````
## INSERT
````

INSERT INTO sites (
name,
beloning,
addedBy,
assignedTo,
time,
design,
typeDomain, 
link)
 VALUES(
 'test.com.ua',
 'Admin',
 'Partner #1',
 'Worker #1',
  '2019-11-12', 
 'someDesign.zip',
 'Active',
 'test.com.ua');

 
INSERT INTO partners (
username,
password,
status,
lastLogin,
percent,
image,
email
)
 VALUES(
 'Dmitry',
 'testpass',
 true,
  '2019-11-12', 
  '1.213',
  'src/asdsa.jpeg',
  'Dmitry@gmail.com'
);




INSERT INTO workers (
username,
password,
status,
ip,
tradesAmount,
lastLogin,
partnerBelonging,

worderInfoId,
image,
email
)
 VALUES(
 'Dmitry',
 'testpass',
 true,
 '195.208.131.1',
 '7 / $813.65',
  '2019-11-12,12:41', 
  '#1',
  22,
  'src/asdsa.jpeg',
  'Dmitry@gmail.com');



INSERT INTO trades (
siteId,
worderId,
steam64,
sumTrade,
items,
tradesDateTime
)
 VALUES(
 12,
 12,
 '2321312321321',
 100,
 '{items:{my:{[itemId: 23]}, partner:{[itemId: 33]}}}',
  '2019-11-12,12:41'
);

INSERT INTO botsSteam (
statusBot,
steamId ,
notHolded ,
holded ,
beloning ,
actionBot 
)
 VALUES(
 true,
 '123123123123',
 '250$ / 1 item(s)',
 '500$ / 2 item(s)',
 'worker#1',
  'some'
);

INSERT INTO sessions (
startTime ,
avatarSrc  ,
status ,
steamId ,
login ,
sessionKey ,
email ,
priceIpventory ,
lastLogin ,
beloning ,
ipAddress ,
tradesWorkerId ,
sessionsWorkerId 
)
 VALUES(
 '2019-11-12',
 'src/some.jpg',
 true,
 '21312312312',
 'loginName',
 'sessionKey',
 'email.com',
 '$978.20',
 '2019-11-12,12:41',
  'Partner#1',
  '195.208.131.1',
  12,
  14
);

`````

